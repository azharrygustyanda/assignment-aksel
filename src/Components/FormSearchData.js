import "../Styles/FormSearchData.css";
import DataTable from "react-data-table-component";
import { Card, Col, Container, Row } from "react-bootstrap";

function FormSearchData(props) {
  const columns = [
    { id: "Number", selector: (row, index) => index + 1 },
    { id: "Ingredient", selector:row => row.strIngredient1 },
    { id: "Measure", selector:row => row.strMeasure1 },
  ];
  
  return (
    <Card className="mt-3 border-0" id="CardData">
      <Card.Body>
        <DataTable 
            data={props.resultSearchData} 
            columns={columns} 
        />
      </Card.Body>
      <Container id="ContainerRowData">
        <Row>
          <Col xs={12} md={8} id="RowDataLabel">
            TOTAL INGREDIENTS
          </Col>
          <Col xs={6} md={4} id="RowDataTotal">
            {props.resultSearchData.length}
          </Col>
        </Row>
      </Container>
    </Card>
  );
}

export default FormSearchData;