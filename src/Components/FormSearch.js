import { Button, Card, Col, Container, FormControl, InputGroup, Row } from 'react-bootstrap';
import { UilGlobe } from '@iconscout/react-unicons';
import React, { useState } from 'react';
import swal from 'sweetalert';
import '../Styles/FormSearch.css';
import FormSearchData from './FormSearchData'

function FormSearch() {
    const [search, setSearch] = useState('');
    const [results, setResults] = useState([]);

    const handleSearch = async () => {
        const res = await fetch(`https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${search}`);
        const data = await res.json();

        if (data.drinks !== null){
            setResults(data.drinks);
        } else {
            swal('NOPE!', 'Data you looking for is unavailable!', 'error');
        }
    }

    const handleChangeIngridients = (e, idx) => {
        let updateData = results.map((item, index) => {
            if (index === idx) {
                return {
                    ...item,
                    [e.target.name]: e.target.value
                }
            }
            return {...item}
        })
        setResults(updateData)
    }

    return (

    <Container id='ContainerCardSearch'>
        <FormSearchData resultSearchData={results} />
        
        <Card className="mt-3 border-0" id='CardSearch'>
            <Container id='ContainerRowSearch'>
                <Row>
                    <Col xs={12} md={8}>
                        <InputGroup>
                            <FormControl 
                                id='InputSearch' 
                                placeholder='search . . .'
                                onChange={e => setSearch(e.target.value)}
                            />
                        </InputGroup>
                    </Col>
                    <Col xs={6} md={4}>
                        <Button variant="outline-dark" id='ButtonSearch' onClick={handleSearch}>
                            <UilGlobe id='icsGlobe'/>
                                SEARCH
                        </Button>
                    </Col>
                </Row>
            </Container>
            <Container id='ContainerRowResult'>
                <Card.Body>
                    <Row>
                        {results && results.map((result, index) => (
                            <React.Fragment key={index}>
                                <Col xs={3} md={1}> 
                                    <p id='resNumber'>{index + 1}</p>
                                </Col>
                                <Col xs={12} md={7}>
                                    <FormControl 
                                        name='strIngredient1' 
                                        id='resIngridients' 
                                        value={result.strIngredient1} 
                                        onChange={(e) => handleChangeIngridients(e, index)} 
                                    />
                                </Col>
                                <Col xs={9} md={4}>
                                    <FormControl 
                                        name='strMeasure1'
                                        id='resMeasure' 
                                        value={result.strMeasure1}
                                        onChange={(e) => handleChangeIngridients(e, index)}
                                    />
                                </Col>
                            </React.Fragment>
                        ))}
                    </Row>
                </Card.Body>
            </Container>
        </Card>
    </Container>
    );
}

export default FormSearch;