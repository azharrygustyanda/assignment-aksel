import React from 'react';
import '../Styles/App.css';
import FormSearch from '../Components/FormSearch';
import { Card, Container } from 'react-bootstrap';

function App() {
  return (
    <Container style={{color:'black'}}>
      <React.StrictMode>
        
        <Container id='ContainerCardHeader'>
              <Card className='border-0' id='CardHeader'>
                  <Card.Body>
                      <h1 style={{textAlign:'center'}}>RECIPE EDITOR</h1>
                  </Card.Body>
              </Card>
          </Container>

        <FormSearch />
      </React.StrictMode>
    </Container>
  );
}

export default App;
